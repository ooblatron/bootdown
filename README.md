# Bootdown - Create websites quickly with just Markdown files

Bootdown allows you to create quick and simple websites with Bootstrap styling using just Markdown files

## Installation

Install the latest version with

```bash
composer create-project ooblatron/bootdown-site .
```

Point your web hosting root to `html`. 

## Basic Usage

Add new pages by uploading Markdown documents to the docs directory and then mapping them to a URL by changing the `config/Map.php` file.

For example

1. Upload a Markdown document called `world.md` to the docs directory
2. Add the following line to `config/Maps.php`

```php
    'hello'  => 'world.md'
```

Going to `https://yourdomain.com/hello` will display the contents of `world.md` with [Bootstrap](https://getbootstrap.com/) styling.

You will probably prefer to have the name of the Markdown file match the URL path but, as the example shows, you do not have to.

You can also map to Markdown files in subdirectories, for example

```php
    'hello'  => 'greetings/world.md'
```

This displays the Markdown file `docs/greetings/world.md` when visiting `https://yourdomain.com/hello`.

### How to use Markdown ###

Please refer to the [Markdown Guide](https://www.markdownguide.org/basic-syntax/) if you need help with formatting documents.

### More information ###

- [Documentation](https://ooblatron.org/bootdown/)
- [Packagist page](https://packagist.org/packages/ooblatron/bootdown)
- [Source code](https://bitbucket.org/ooblatron/bootdown)

## Documentation

- [Please visit the Bootdown website](https://ooblatron.org/bootdown)

## About

### Requirements

- PHP 7.1 or above
- Composer package [ooblatron/slipstream]() for dependency injection
- Composer package [erusev/parsedown]() to convert Markdown to HTML
- Composer package [ooblatron/tsuka]() to top and tail web pages and add Bootstrap styling
 
### Bootstrap versions

Version 1 of Bootdown supports Bootstrap 4.

### Author

Tim Rogers - <tim@ooblatron.org> - <https://twitter.com/timdrogers><br />

### License

Bootdown is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

### Acknowledgements

This package uses the brilliant [Bootstrap](https://getbootstrap.com/) framework, which allows back-end coders to create clean and professional web pages without needing a deep knowledge of graphic design or CSS.

The graphics used to display HTTP error codes are from [Laravel version 5.7](https://github.com/laravel/laravel/tree/5.7/public/svg).

The .htaccess syntax was taken from a [Stackoverflow answer](https://stackoverflow.com/a/13760676/2511355) provided by user [jacoz](https://stackoverflow.com/users/792624/jacoz).