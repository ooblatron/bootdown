### 1.0.0-RC1 (2022-11-28)

* Initial release candidate

### 1.0.0 (2022-11-28)

* Initial release

### 1.0.1 (2022-11-28)

* Fixed installation instructions.

### 1.0.2 (2022-11-28)

* Fixed wrong package name in Licence section of README

### 1.0.3 (2022-12-07)

* Added .env file and example files

### 1.0.4 (2022-12-07)

* Added ability to specify viewports to display before and after

### 1.1 (2022-12-07)

* Changed to a library and moved project components out into a new Composer package bootdown-site

### 1.1.1 (2022-12-08)

* Fixed not found when path ends in forward slash

### 1.1.2 (2022-12-09)

* Added .env.example
* Improved exception handling

### 1.1.3 (2022-12-09)

* Added ability to turn off page container