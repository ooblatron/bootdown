<?php

namespace Bootdown;

use Slipstream\SlipstreamFactory;
use Throwable;
use function http_response_code;

class Bootdown
{
    /**
     * @var SlipstreamFactory
     */
    private $slipstreamFactory;
    /**
     * @var Renderer
     */
    private $renderer;
    /**
     * @var ServerError
     */
    private $serverError;
    /**
     * @var Environment
     */
    private $environment;

    public function __construct(
        Renderer $renderer,
        ServerError $serverError,
        SlipstreamFactory $slipstreamFactory,
        Environment $environment
    ) {
        $this->renderer = $renderer;
        $this->serverError = $serverError;
        $this->slipstreamFactory = $slipstreamFactory;
        $this->environment = $environment;
    }
    
    public function go(): void
    {
        try {
            try {
                $map = $this->slipstreamFactory->get('Map');
            } catch (Throwable $e) {
                $map = $this->slipstreamFactory->get('MapExample');
            }
            $this->renderer->setMap($map);
            $this->renderer->display();
        } catch (Throwable $e) {
            http_response_code(500);
            try {
                $isDevelopmentEnvironment = $this->environment->getIsDevelopmentEnvironment();
                $errorMessage = null;
                if ($isDevelopmentEnvironment) {
                    $errorMessage = $e;
                }
                $this->serverError->build($errorMessage)->display();
            } catch (Throwable $e) {
                http_response_code(500);
                echo '<h1>500 Server Error</h1><hr>';
                exit($e);
            }
        }
    }
}