<?php

namespace Bootdown;

use Exception;

class ErrorPage extends Viewport
{
    /**
     * @throws Exception
     */
    protected function addError(
        string $title,
        string $error,
        string $backgroundImage,
        ?string $errorMessage = null
    ): void {
        $this->startHtml();
        $this->addHead($title);
        $this->startBody(false);
        $this->startCard(['class' => 'text-white']);
        $source = $this->getImageSourceFromFile(__DIR__ . '/svg/' . $backgroundImage);
        $this->startCardOverlay($source, $title);
        $this->addCardTitle($error, ['class' => 'display-4']);
        if ($errorMessage) {
            $this->startCardBody();
            $this->addLead($errorMessage);
            $this->endCardBody();
        }
        $this->endCardOverlay();
        $this->endCard();
        $this->endBody();
    }
}