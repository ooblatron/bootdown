<?php

namespace Bootdown;

use Dotenv\Dotenv;

class Environment
{
    private const DEFAULT_DOCS_DIRECTORY = '../docs';
    private const CONFIG_FILE_PATH = '../config';

    /**
     * @var Dotenv
     */
    private $parameterFile;

    public function __construct() {
        $this->parameterFile = Dotenv::createImmutable(self::CONFIG_FILE_PATH);
        $this->parameterFile->safeLoad();
        $this->parameterFile->ifPresent('DEV')->isBoolean();
    }

    public function getDocsPath(): string {
        return ($_ENV['DOC_PATH'] ?? self::DEFAULT_DOCS_DIRECTORY) . '/';
    }

    public function getUriPath(): string {
        $path = $_GET['path'] ?? null;
        if ($path) {
            $path = rtrim($path,'/');
        } else {
            $path = '/';
        }

        return $path;
    }

    public function getViewportBefore(): ?string {
        return $_ENV['VIEWPORT_BEFORE'] ?? null;
    }

    public function getViewportAfter(): ?string {
        return $_ENV['VIEWPORT_AFTER'] ?? null;
    }

    public function getIsDevelopmentEnvironment(): bool {
        return $this->isTrue('DEV', false);
    }

    public function getIncludeContainer(): bool
    {
        return $this->isTrue('PAGE_CONTAINER', true);
    }

    private function isTrue(
        string $key,
        ?bool $defaultValue = null
    ):bool {
        $value = $_ENV[$key] ?? null;
        if (is_null($value)) {
            $result = $defaultValue;
        } else {
            switch ($value) {
                case 1:
                case 'on':
                case 'true':
                case 'yes':
                    $result = true;
                    break;
                case 0:
                case 'off':
                case 'false':
                case 'no':
                    $result = false;
                    break;
                default:
                    $result = $defaultValue;
                    break;
            }
        }

        return $result;
    }
}