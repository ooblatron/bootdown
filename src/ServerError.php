<?php

namespace Bootdown;

use Exception;

class ServerError extends ErrorPage
{
    /**
     * @throws Exception
     */
    public function build(?string $errorMessage): ServerError {
        $this->addError(
            'Server Error',
            '505 Server Error',
            '500.svg',
            $errorMessage
        );
        return $this;
    }
}