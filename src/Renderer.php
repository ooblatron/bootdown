<?php

namespace Bootdown;

use Exception;
use Parsedown;
use ReflectionException;
use Slipstream\SlipstreamFactory;

class Renderer
{
    private $map;
    /**
     * @var NotFound
     */
    private $notFound;
    /**
     * @var Viewport
     */
    private $viewport;
    /**
     * @var Parsedown
     */
    private $parsedown;
    /**
     * @var Environment
     */
    private $environment;
    /**
     * @var SlipstreamFactory
     */
    private $slipstreamFactory;

    /**
     * @throws Exception
     */
    public function __construct(
        Viewport $viewport,
        Parsedown $parsedown,
        NotFound $notFound,
        Environment $environment,
        SlipstreamFactory $slipstreamFactory
    ) {
        $this->viewport = $viewport;
        $this->parsedown = $parsedown;
        $this->notFound = $notFound;
        $this->environment = $environment;
        $this->slipstreamFactory = $slipstreamFactory;
    }

    /**
     * @throws Exception
     */
    public function display(): void {
        $key = $this->environment->getUriPath();
        $document = $this->map::MAP[$key] ?? null;
        if ($document) {
            $file = file_get_contents($this->environment->getDocsPath() . $document);
            if ($file === false) {
                $this->notFound($this->notFound);
            } else {
                $includeContainer = $this->environment->getIncludeContainer();
                $viewportBeforeClass = $this->environment->getViewportBefore();
                $viewportAfterClass = $this->environment->getViewportAfter();
                $this->viewport->startHtml();
                $this->viewport->addHead($key);
                $this->viewport->startBody($includeContainer);
                $this->append($viewportBeforeClass);
                $this->viewport->append([$this->parsedown->text($file)]);
                $this->append($viewportAfterClass);
                $this->viewport->endPage();
                $this->viewport->display();
            }
        } else {
            $this->notFound($this->notFound);
        }
    }

    /**
     * @throws ReflectionException
     */
    private function append(?string $viewportClassName): void {
        if ($viewportClassName) {
            $viewportBefore = $this->slipstreamFactory->get($viewportClassName);
            $this->viewport->append($viewportBefore->build());
        }
    }
    
    public function setMap($map): void {
        $this->map = $map;
    }

    public function setFactory(SlipstreamFactory $slipstreamFactory): void {
        $this->slipstreamFactory = $slipstreamFactory;
    }

    /**
     * @throws Exception
     */
    private function notFound(NotFound $notFound): void {
        $notFound->build()->display();
    }
}