<?php

namespace Bootdown;

use Exception;

class NotFound extends ErrorPage
{
    /**
     * @throws Exception
     */
    public function build(): NotFound {
        $this->addError(
            'Not Found',
            '404 Not Found',
            '404.svg'
        );
        return $this;
    }
}